#!/bin/bash

display_tree() {
    local directory="$1"
    local indent="$2"
    local output_file="$3"
    local ignore_folders=("$4")

    if [ -z "$output_file" ]; then
        output_file="output.txt"
    fi

    folder_name=$(basename "$directory")
    if [[ ! " ${ignore_folders[@]} " =~ " $folder_name " ]]; then
        echo "$indent----$folder_name/" >> "$output_file"
    fi

    indent+="    |"

    for item in "$directory"/*; do
        if [ -d "$item" ]; then
            item_name=$(basename "$item")
            if [[ ! " ${ignore_folders[@]} " =~ " $item_name " ]]; then
                display_tree "$item" "$indent" "$output_file" "${ignore_folders[@]}"
            fi
        else
            if [[ ! " ${ignore_folders[@]} " =~ " $folder_name " ]]; then
                echo "$indent----$(basename "$item")" >> "$output_file"
            fi
        fi
    done
}

#! ==================================================
#!  INPUT
root_directory="../../"
output_file="tree.txt"
ignore_folders=("node_modules" "__pycache__" ".git")
#!  INPUT
#! ==================================================

echo "Cấu trúc thư mục dự án của tôi: " > "$output_file"
echo -e "\n\n\n" >> "$output_file"

display_tree "$root_directory" "" "$output_file" "${ignore_folders[@]}"
echo "Kết quả đã được ghi vào tập tin $output_file"

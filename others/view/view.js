#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

function displayTree(directory, indent = '', output_file = null, ignore_folders = null) {
    if (!output_file) {
        output_file = 'output.txt';
    }

    if (!ignore_folders) {
        ignore_folders = ['node_modules', '__pycache__', '.git'];
    }

    const folder_name = path.basename(directory);
    if (!ignore_folders.includes(folder_name)) {
        fs.appendFileSync(output_file, indent + '----' + folder_name + '/\n', 'utf-8');
    }

    indent += '    |';

    const items = fs.readdirSync(directory);
    for (const item of items) {
        const item_path = path.join(directory, item);
        if (fs.statSync(item_path).isDirectory()) {
            if (!ignore_folders.includes(path.basename(item_path))) {
                displayTree(item_path, indent, output_file, ignore_folders);
            }
        } else {
            if (!ignore_folders.includes(folder_name)) {
                fs.appendFileSync(output_file, indent + '----' + item + '\n', 'utf-8');
            }
        }
    }
}

//! ==================================================
//! INPUT
const root_directory = '../../';
const output_file = 'tree.txt';
const ignore_folders = ['node_modules', '__pycache__', '.git'];
//! INPUT
//! ==================================================

fs.writeFileSync(output_file, 'Cấu trúc thư mục dự án của tôi: \n\n\n', 'utf-8');
displayTree(root_directory, '', output_file, ignore_folders);
console.log(`Kết quả đã được ghi vào tập tin ${output_file}`);

#!/usr/bin/env python
import os

def display_tree(directory, indent='', output_file=None, ignore_folders=None):
    if not output_file:
        output_file = "output.txt"

    if ignore_folders is None:
        ignore_folders = ["node_modules", "__pycache__", ".git"]

    folder_name = os.path.basename(directory)
    if folder_name not in ignore_folders:
        with open(output_file, "a",encoding="utf-8") as file:
            file.write(indent + "----" + folder_name + "/\n")

    indent += "    |"

    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)
        if os.path.isdir(item_path):
            if os.path.basename(item_path) not in ignore_folders:
                display_tree(item_path, indent, output_file, ignore_folders)
        else:
            if folder_name not in ignore_folders:
                with open(output_file, "a",encoding="utf-8") as file:
                    file.write(indent + "----" + item + "\n")

#! ==================================================
#! INPUT
root_directory = r"../../"
output_file = "tree.txt"
ignore_folders = ["node_modules", "__pycache__", ".git"]
#! INPUT
#! ==================================================
with open(output_file, 'w',encoding="utf-8") as file:
    file.write(f"Cấu trúc thư mục dự án của tôi: \n\n\n")
display_tree(root_directory, output_file=output_file, ignore_folders=ignore_folders)
print(f"Kết quả đã được ghi vào tập tin {output_file}")
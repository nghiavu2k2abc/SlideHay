import os
import subprocess


def format2space(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.ts'):
                file_path = os.path.join(root, file)

                with open(file_path, 'r+', encoding="utf-8") as f:
                    contents = f.read()
                    while "  " in contents:
                        contents = contents.replace("  ", " ")
                    f.seek(0)
                    f.write(contents)
                    f.truncate()


def format(directory, type_files):
    sum = 0
    for root, dirs, files in os.walk(directory):
        for file in files:
            for file_extension in type_files:
                if file.endswith(file_extension):
                    file_path = os.path.join(root, file)
                    subprocess.run(
                        [r"%APPDATA%\npm\prettier.cmd", "--write", file_path],
                        shell=True,
                        env={**os.environ, "APPDATA": os.environ["APPDATA"]},
                    )
                    sum += 1
    return sum


type_files = [".ts"]
try:
    subprocess.run(" npm i -g prettier", shell=True, check=True)
    pass
except subprocess.CalledProcessError as e:
    print(f"Lỗi [Prettier]: {e}")
os.chdir("../../microservices")
path = os.getcwd()

links = [
    f"{path}\\common\\src",
    f"{path}\\notifications\\src",
    f"{path}\\mails\\src",
    f"{path}\\nghia\\src",
    f"{path}\\users\\src",
    f"{path}\\products\\src",
    f"{path}\\carts\\src",
    f"{path}\\invoices\\src",
    f"{path}\\apigatewaydemo\\src",
]

for link in links:
    # print(link)
    format2space(link)
    format(link, type_files)
    sum_file = format(link, type_files)
print(f"\nXong format: {sum_file} file")

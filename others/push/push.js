#!/usr/bin/env node
const { execSync } = require("child_process");
const fs = require("fs");
const path = require("path");

const currentDate = new Date();
const message = currentDate.toISOString().replace(/[-T:Z.]/g, "_");
const currentDirectory = process.cwd();
console.log(currentDirectory);

const targetDirectory = path.join(currentDirectory, "../../");
process.chdir(targetDirectory);

execSync("git add .", { stdio: "inherit" });
execSync(`git commit -m "${message}"`, { stdio: "inherit" });
execSync("git push", { stdio: "inherit" });

console.log("Xong");

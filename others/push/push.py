#!/usr/bin/env python
import os
import subprocess
from datetime import datetime

message = datetime.now().strftime('%H_%M_%S_%f___%d_%m_%Y')
current_directory = os.getcwd()
print(current_directory)

os.chdir("../../")

subprocess.run(["git", "add", "."])
subprocess.run(["git", "commit", "-m", f"{message}"])
subprocess.run(["git", "push"])

print("Xong")

#!/usr/bin/env python
import os
import subprocess

try:
    os.chdir("others/python")
    subprocess.run(['pip', 'freeze'], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    with open('requirements.txt', 'w') as file:
        subprocess.run(['pip', 'freeze'], check=True, stdout=file, stderr=subprocess.PIPE)
    print("Đã tạo file requirements.txt thành công!")
except subprocess.CalledProcessError as e:
    print(f"Lỗi trong quá trình tạo file requirements.txt: {e.stderr.decode('utf-8')}")

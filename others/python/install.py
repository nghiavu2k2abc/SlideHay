#!/usr/bin/env python
import os
import subprocess

try:
    os.chdir("others/python")
    subprocess.run(['pip', 'install', '-r', 'requirements.txt'], check=True)
    print("Đã cài đặt các package từ requirements.txt thành công!")
except subprocess.CalledProcessError as e:
    print(f"Lỗi trong quá trình cài đặt các package: {e.stderr.decode('utf-8')}")
